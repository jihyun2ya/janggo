package com.thunbi.janggo.model
import com.thunbi.janggo.data.Item

/**
 * Created by thunder on 2017. 2. 21..
 */

interface ItemModel {
    fun addItem(item: Item)
    fun getItem(position: Int): Item?
    fun removeItem(item: Item)
    fun getItemCount(): Int
}