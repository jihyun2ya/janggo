package com.thunbi.janggo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.thunbi.janggo.view.MainFragment
import com.thunbi.janggo.util.replaceFragmentToActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragmentToActivity(MainFragment.getInstance(), R.id.frame_layout)
    }
}
