package com.thunbi.janggo.view

import com.thunbi.janggo.R
import android.support.v4.app.Fragment
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.thunbi.janggo.ItemAdapter
import com.thunbi.janggo.presenter.ItemContract
import com.thunbi.janggo.presenter.ItemPresenter

/**
 * Created by thunder on 2017. 2. 21..
 */
class MainFragment : Fragment(), ItemContract.View {

    companion object {
        fun getInstance() = MainFragment()
    }

    private var itemPresenter: ItemPresenter? = null
    private var itemAdapter: ItemAdapter? = null

    private val recyclerView by lazy {
        view?.findViewById(R.id.recycler_view) as RecyclerView
    }

    private val btnAdd by lazy {
        view?.findViewById(R.id.btn_add) as Button
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        itemPresenter = ItemPresenter()
        itemPresenter?.view = this

        itemAdapter = ItemAdapter(context)
        itemPresenter?.itemModel = itemAdapter
        recyclerView?.adapter = itemAdapter

        itemPresenter?.loadDefaultItems()

        btnAdd.setOnClickListener {
            itemPresenter?.adapterOneAddItem()
        }
    }

    override fun adapterOneNotify() {
        itemAdapter?.notifyDataSetChanged()
    }

    override fun onSuccessAddItem(position: Int) {
//        Toast.makeText(context, "아이템 추가 완료" + position, Toast.LENGTH_SHORT).show()
        recyclerView.scrollToPosition(position)
    }

    override fun onSuccessRemoveItem(position: Int) {
//        Toast.makeText(context, "아이템 제거 완료", Toast.LENGTH_SHORT).show()
    }
}