package com.thunbi.janggo

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.Toast
import com.thunbi.janggo.data.Item
import com.thunbi.janggo.holder.ItemViewHolder
import com.thunbi.janggo.model.ItemModel
import java.util.*

/**
 * Created by thunder on 2017. 2. 21..
 */

class ItemAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), ItemModel {

    val itemList: MutableList<Item> = ArrayList()
    // 뷰 생성 시 최초 한 번
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return ItemViewHolder(context, parent)
    }

    // 뷰 갱신 시
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
//        when (getItem(position)){
            (holder as ItemViewHolder)?.bindView(getItem(position), position)
//        }
    }

    override fun getItemCount() = itemList.size

    override fun addItem(item: Item) {
        itemList.add(item)
//        Toast.makeText(context, item.name, Toast.LENGTH_SHORT).show()
    }

    override fun getItem(position: Int) = itemList[position]

    override fun removeItem(item: Item) {
        itemList.remove(item)
    }
}