package com.thunbi.janggo.presenter

import com.thunbi.janggo.data.Item
import com.thunbi.janggo.model.ItemModel

/**
 * Created by thunder on 2017. 2. 21..
 */

class ItemPresenter : ItemContract.Presenter {

    override var view: ItemContract.View? = null
    override var itemModel: ItemModel? = null

    override fun loadDefaultItems() {
        for (position in 0..5) {
            itemModel?.addItem(Item("name" + position, "ca", "photo", null, "1", "2"))
        }
        view?.adapterOneNotify()
    }

    override fun adapterOneAddItem() {
        itemModel?.addItem(Item("name2222", "ca", "photo", null, "1", "2"))
        itemModel?.addItem(Item("name3333", "ca", "photo", null, "1", "2"))
        view?.adapterOneNotify()
        view?.onSuccessAddItem(itemModel!!.getItemCount())
    }
}