package com.thunbi.janggo.presenter

import com.thunbi.janggo.model.ItemModel

/**
 * Created by thunder on 2017. 2. 21..
 */

interface ItemContract {

    interface View {
        fun adapterOneNotify()
        fun onSuccessAddItem(position: Int)
        fun onSuccessRemoveItem(position: Int)
    }

    interface Presenter {
        var view: View?
        var itemModel: ItemModel?
        fun loadDefaultItems()
        fun adapterOneAddItem()
    }
}

