package com.thunbi.janggo.data

import android.graphics.drawable.BitmapDrawable

/**
 * Created by thunder on 2017. 2. 21..
 */
data class Item(var name: String,
                var category: String,
                var photoUrl: String,
                var photo: BitmapDrawable?,
                var expireDate: String,
                var incomingDate: String)