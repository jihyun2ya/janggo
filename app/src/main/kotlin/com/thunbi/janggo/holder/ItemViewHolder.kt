package com.thunbi.janggo.holder

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.thunbi.janggo.R
import com.thunbi.janggo.data.Item

/**
 * Created by thunder on 2017. 2. 21..
 */

class ItemViewHolder(val context: Context, parent: ViewGroup?) :
        RecyclerView.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_view, parent, false)) {

    private val textView by lazy {
        itemView?.findViewById(R.id.item_name) as TextView
    }

    fun bindView(item: Item?, position: Int) {
        itemView?.setOnClickListener {
            
        }
        textView.text = item?.name
    }
}